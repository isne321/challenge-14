#include<iostream>
using namespace std;

void quicksort(int[], int, int);
int partition(int[], int, int);
void mergesort(int[], int);

int main() {
	const int size = 15;
	int arr[size] = { 16,4,6,12,3,24,10,8,2,5,45,7,50,99,13 };
	const int first = arr[0];
	const int last = arr[size - 1];

	//quicksort(arr, first, last);
	mergesort(arr, size);
	for (int i = 0; i < size; i++) { //prints out
		cout << arr[i] << " ";
	}
	return 0;
}

void quickSort(int arr[], int size) {
	int left = arr[size-1];
	int right = arr[0];
	int i = left, j = right;
	int tmp;
	int pivot = arr[(left + right) / 2];

	/* partition */
	while (i <= j) {
		while (arr[i] < pivot)
			i++;
		while (arr[j] > pivot)
			j--;
		if (i <= j) {
			tmp = arr[i];
			arr[i] = arr[j];
			arr[j] = tmp;
			i++;
			j--;
		}
	};
	
	/* recursion */
	if (left < j) {
		quickSort(arr, left, j);
	}

	if (i < right) {
		quickSort(arr, i, right);
	}		
}

void mergesort(int arr[], int size) {
	int first = 0, last = size - 1;
	if (first < last)
	{
		int mid = size / 2;
		mergesort(arr, mid);
		quicksort(arr, first, last);
	}

}